# nextcloud_docker

## setup 
before starting the app we need to create self signed certificates:

```shell 
docker-compose --profile cert up
```

afterwards we can start the application with:
```shell 
docker-compose --profile app up -d
```
This is the only thing we need to start after a restart or upgrade




